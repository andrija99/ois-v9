/* global $ */

$(document).ready(function () {
    var baseUrl = "https://teaching.lavbic.net/OIS/chat/api";
    var user = {id: 33, name: "Andrija Kuzmanov"}; // TODO: vnesi svoje podatke
    var nextMessageId = 0;
    var currentRoom = "Skedenj";


    // Naloži seznam sob
    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (var i in data) {
                $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                </a> \
                <div class='media-body'> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
            }
        }
    });


    // TODO: Naloga
    // Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund

    var updateChat = function () {
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: "get",

            success: function (data) {
                for (var i in data) {
                    var message = data[i];
                    $("#messages").append(" \
            <li class='media'> \
              <div class='media-body'> \
                        <div class='media'> \
                  <a class='pull-left' href='#'> \
                    <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + message.user.id + ".jpg'> \
            </a> \
      <div class='media-body'> \
      <small class='text-muted'>" + message.user.name
                        + " | " + message.time
                        + "</small> <br> \
              " + message.text
                        + " \
                    <hr> \
                  </div> \
                </div> \
              </div> \
            </li>");
                    nextMessageId = message.id + 1;
                }
                setTimeout(function () {
                        updateChat()
                    }, 5000
                );
            }
        });
    };

    // Klic funkcije za začetek nalaganja pogovorov
    updateChat();

    // TODO: Naloga
    // Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    var updateUsers = function () {
        $.ajax({
            url: baseUrl + "/users/" + currentRoom,
            type: "get",
            success: function (data) {
                $("#users").html("");
                for (var i in data) {
                    var user = data[i];
                    $("#users").append(" \
                    <li class='media'> \
                      <div class='media-body'> \
                        <div class='media'> \
                            <a class='pull-left' href='#'> \
                            <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + user.id + ".jpg' /> \
                    </a> \
                    <div class='media-body' > \
                    <h5>" + user.name + "</h5> \
                          </div> \
                        </div> \
                      </div> \
                    </li>");
                }
                setTimeout(function () {
                        updateUsers();
                    }, 5000
                );
            }
        });
    };

    // Klic funkcije za začetek posodabljanja uporabnikov
    updateUsers();

    // TODO: Naloga
    // Definicija funkcije za pošiljanje sporočila
    var sendMessage = function () {
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom,
            type: "post",
            contentType: 'application/json',

            data: JSON.stringify({

                user: user,
                text: $("#message").val()}),

            success: function (data) {
            $("#message").val("");
        },
        error: function(err) {
            alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!");
        }
    });
    };

    // On Click handler za pošiljanje sporočila
    $("#send").click(sendMessage);

    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (var i in data) {
                $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='img/" + data[i] + ".jpg'> \
                </a> \
                <div class=\"media-body\"> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
            }
            $(".room").click(changeRoom);
        }
    });

    // TODO: Naloga
    // Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    // V razmislek: pomisli o morebitnih težavah!
    var changeRoom = function (event) {
        $("#messages").html("");
        $("#users").html("");
        currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
        nextMessageId = 0;
    };

    // On Click handler za menjavo sobe
    // Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.
});
